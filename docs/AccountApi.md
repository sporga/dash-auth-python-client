# dash_auth.AccountApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_account_by_id_get**](AccountApi.md#api_account_by_id_get) | **GET** /api/Account/{id} | 
[**api_account_get**](AccountApi.md#api_account_get) | **GET** /api/Account | 
[**api_account_post**](AccountApi.md#api_account_post) | **POST** /api/Account | 


# **api_account_by_id_get**
> User api_account_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.AccountApi(dash_auth.ApiClient(configuration))
id = 'id_example' # str | 

try:
    api_response = api_instance.api_account_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountApi->api_account_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**User**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_account_get**
> list[User] api_account_get(user_ids=user_ids, email_addresses=email_addresses)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.AccountApi(dash_auth.ApiClient(configuration))
user_ids = ['user_ids_example'] # list[str] |  (optional)
email_addresses = ['email_addresses_example'] # list[str] |  (optional)

try:
    api_response = api_instance.api_account_get(user_ids=user_ids, email_addresses=email_addresses)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountApi->api_account_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_ids** | [**list[str]**](str.md)|  | [optional] 
 **email_addresses** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**list[User]**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_account_post**
> api_account_post(value=value)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.AccountApi(dash_auth.ApiClient(configuration))
value = dash_auth.Register() # Register |  (optional)

try:
    api_instance.api_account_post(value=value)
except ApiException as e:
    print("Exception when calling AccountApi->api_account_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | [**Register**](Register.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

