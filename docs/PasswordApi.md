# dash_auth.PasswordApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_account_password_post**](PasswordApi.md#api_account_password_post) | **POST** /api/account/Password | 
[**api_account_password_put**](PasswordApi.md#api_account_password_put) | **PUT** /api/account/Password | 


# **api_account_password_post**
> api_account_password_post(value=value)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.PasswordApi(dash_auth.ApiClient(configuration))
value = dash_auth.UserQuery() # UserQuery |  (optional)

try:
    api_instance.api_account_password_post(value=value)
except ApiException as e:
    print("Exception when calling PasswordApi->api_account_password_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | [**UserQuery**](UserQuery.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_account_password_put**
> api_account_password_put(value=value)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.PasswordApi(dash_auth.ApiClient(configuration))
value = dash_auth.ChangePasswordRequest() # ChangePasswordRequest |  (optional)

try:
    api_instance.api_account_password_put(value=value)
except ApiException as e:
    print("Exception when calling PasswordApi->api_account_password_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | [**ChangePasswordRequest**](ChangePasswordRequest.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

