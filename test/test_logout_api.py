# coding: utf-8

"""
    Dash Auth API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: v1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import dash_auth
from dash_auth.api.logout_api import LogoutApi  # noqa: E501
from dash_auth.rest import ApiException


class TestLogoutApi(unittest.TestCase):
    """LogoutApi unit test stubs"""

    def setUp(self):
        self.api = dash_auth.api.logout_api.LogoutApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_api_logout_by_logout_id_get(self):
        """Test case for api_logout_by_logout_id_get

        """
        pass


if __name__ == '__main__':
    unittest.main()
