# Register

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **str** |  | [optional] 
**surname** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**gender** | **str** |  | [optional] 
**address** | **str** |  | [optional] 
**suburb** | **str** |  | [optional] 
**state** | **str** |  | [optional] 
**mobile_phone_number** | **str** |  | [optional] 
**password** | **str** |  | [optional] 
**postcode** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


