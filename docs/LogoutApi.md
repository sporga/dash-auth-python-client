# dash_auth.LogoutApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_logout_by_logout_id_get**](LogoutApi.md#api_logout_by_logout_id_get) | **GET** /api/Logout/{logoutId} | 


# **api_logout_by_logout_id_get**
> Logout api_logout_by_logout_id_get(logout_id)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.LogoutApi(dash_auth.ApiClient(configuration))
logout_id = 'logout_id_example' # str | 

try:
    api_response = api_instance.api_logout_by_logout_id_get(logout_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LogoutApi->api_logout_by_logout_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logout_id** | **str**|  | 

### Return type

[**Logout**](Logout.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

