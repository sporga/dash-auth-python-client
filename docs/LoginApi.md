# dash_auth.LoginApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_login_auth_external_get**](LoginApi.md#api_login_auth_external_get) | **GET** /api/Login/auth-external | 
[**api_login_post**](LoginApi.md#api_login_post) | **POST** /api/Login | 
[**api_login_providers_get**](LoginApi.md#api_login_providers_get) | **GET** /api/Login/providers | 
[**api_login_request_external_get**](LoginApi.md#api_login_request_external_get) | **GET** /api/Login/request-external | 


# **api_login_auth_external_get**
> api_login_auth_external_get()



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.LoginApi(dash_auth.ApiClient(configuration))

try:
    api_instance.api_login_auth_external_get()
except ApiException as e:
    print("Exception when calling LoginApi->api_login_auth_external_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_login_post**
> api_login_post(model=model)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.LoginApi(dash_auth.ApiClient(configuration))
model = dash_auth.UserLogin() # UserLogin |  (optional)

try:
    api_instance.api_login_post(model=model)
except ApiException as e:
    print("Exception when calling LoginApi->api_login_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**UserLogin**](UserLogin.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_login_providers_get**
> list[AuthenticationScheme] api_login_providers_get()



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.LoginApi(dash_auth.ApiClient(configuration))

try:
    api_response = api_instance.api_login_providers_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LoginApi->api_login_providers_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[AuthenticationScheme]**](AuthenticationScheme.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_login_request_external_get**
> api_login_request_external_get(provider=provider, return_url=return_url)



### Example
```python
from __future__ import print_function
import time
import dash_auth
from dash_auth.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_auth.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_auth.LoginApi(dash_auth.ApiClient(configuration))
provider = 'provider_example' # str |  (optional)
return_url = 'return_url_example' # str |  (optional)

try:
    api_instance.api_login_request_external_get(provider=provider, return_url=return_url)
except ApiException as e:
    print("Exception when calling LoginApi->api_login_request_external_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider** | **str**|  | [optional] 
 **return_url** | **str**|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

